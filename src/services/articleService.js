import { api } from "../boot/axios";
import { slugify } from "./toolsService";
export async function uploadArticle(author, title, meta_title, categories, content, description, preview_img) {
  const slug = slugify(title.toLowerCase(), "-");
  let formData = new FormData();
  formData.append("author", author);
  formData.append("slug", slug);
  formData.append("title", title);
  formData.append("meta_title", meta_title);
  formData.append("content", content);
  formData.append("description", description);
  formData.append("preview_img", preview_img);

  for (let i = 0; i < categories.length; i++) {
    formData.append("categories[]", categories[i]);
  }
  const response = await api.post("blogpost-service/createpost", formData, {
    headers: { "Content-Type": "" },
  });
  return response.data.success;
}
export async function getArticles(sort, category, page) {
  const response = await api.post("blogpost-service/getposts", {
    sort: sort,
    category: category,
    start: Number(page),
    limit: 5,
  });
  return response.data.posts;
}
export async function getFullArticle(slug) {
  const response = await api.post("blogpost-service/getfullpost", { slug: slug });
  return response.data.post;
}
export async function deleteArticle(articleId) {
  const response = await api.post("blogpost-service/deletepost", { post_id: articleId });
  return response.data.success;
}
export async function getShowcase() {
  const response = await api.post("blogpost-service/getshowcase");
  return response.data;
}
export async function setShowcase(articleId) {
  const response = await api.post("blogpost-service/setshowcase", { post_id: articleId });
  return response.data;
}
export async function like(articleId) {
  const response = await api.post("blogpost-service/like", { post_id: articleId });
  return response.data.success;
}
