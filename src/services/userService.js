import { api } from "../boot/axios";

export async function auth({ email, password }) {
  const response = await api.post("user-service/login", { email, password });
  return response.data.token;
}
export async function logout() {
  const response = await api.post("user-service/logout");
  return response.data;
}
export async function getCurrentUser(token) {
  const response = await api.post("user-service/getCurrentUser", { token });
  return response.data.user;
}
