import { api } from "../boot/axios";
export async function getComments(articleId) {
  const response = await api.post("comments-service/getcomments", {
    post_id: articleId,
  });
  return response.data.comments;
}
export async function leaveComent(articleId, parentComment, author, content) {
  const response = await api.post("comments-service/leavecomment", {
    post_id: articleId,
    parent_id: parentComment,
    author: author,
    content: content,
  });
  return response.data;
}

export async function deleteComment(commentId) {
  const response = await api.post("comments-service/deletecomment", { comment_id: commentId });
  return response.data.success;
}
