import { getCurrentUser, logout } from "../../services/userService";
export default {
  namespaced: true,
  state: {
    isLoggedIn: false,
    userData: {},
  },
  getters: {
    userData(state) {
      return state.userData;
    },
    isLoggedIn(state) {
      return state.isLoggedIn;
    },
  },
  mutations: {
    setUser(state, userObject) {
      state.userData = userObject;
      state.isLoggedIn = true;
    },
    removeUser(state) {
      state.userData = {};
      state.isLoggedIn = false;
    },
  },
  actions: {
    async getUserProfile({ commit }) {
      const userObject = await getCurrentUser();
      if (userObject) {
        commit("setUser", userObject);
      }
    },
    async logout({ commit, dispatch }) {
      const response = await logout();
      if (response) {
        commit("removeUser");
        localStorage.removeItem("auth_token");
      }
    },
  },
};
