import { boot } from "quasar/wrappers";
import axios from "axios";
import { Notify } from "quasar";
import { routerInstance as router } from "src/router";

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const api = axios.create({ baseURL: process.env.API_URL });
const UNAUTHORIZED_ERROR_CODES = ["AUTH_ERROR"];

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api;
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
});

api.interceptors.request.use((config) => {
  const token = localStorage.getItem("auth_token");
  config.headers.Authorization = token;
  return config;
});

api.interceptors.response.use((response) => {
  if (response.data && response.data.success === false) {
    const errorData = {
      message: response.data.message,
      code: response.data.code,
    };
    Notify.create({
      type: "negative",
      message: errorData.message,
      position: "top-right",
    });
    if (UNAUTHORIZED_ERROR_CODES.indexOf(errorData.code) >= 0) {
      localStorage.removeItem("auth_token");
      router.push({ name: "Auth" }).catch((err) => {});
    }
  }
  return response;
});

export { api };
