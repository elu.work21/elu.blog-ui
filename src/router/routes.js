const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        name: "Index",
        path: "",
        component: () => import("pages/Index.vue"),
        meta: { requiresAuth: false, requiresAuthCheck: true },
      },
      {
        name: "Article",
        component: () => import("pages/ArticleFullView"),
        path: "article/:slug",
        meta: { requiresAuth: false, requiresAuthCheck: true },
      },
      {
        name: "Auth",
        path: "auth",
        component: () => import("pages/Auth.vue"),
        meta: { requiresAuth: false, requiresAuthCheck: false },
      },
      {
        name: "CreateArticle",
        path: "create-article",
        component: () => import("pages/CreateArticle.vue"),
        meta: { requiresAuth: true, requiresAuthCheck: true },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
