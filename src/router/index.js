import { route } from "quasar/wrappers";
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";
import routes from "./routes";
import { storeInstance as store } from "../store/index";

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */
let routerInstance = null;
export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === "history"
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,
    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(
      process.env.MODE === "ssr" ? void 0 : process.env.VUE_ROUTER_BASE
    ),
  });

  Router.beforeEach(async (to, from, next) => {
    // store.commit("assets/togglePageLoading", true);
    if (
      to.matched.some((record) => record.meta.requiresAuth) &&
      to.matched.some((record) => record.meta.requiresAuthCheck)
    ) {
      // if reqAuth & reqAuthChecked => { if token === undefined => logout;
      // if token != undefined => check token, if checkTokenResult == false => {logout} else { goNext() } }
      if (!localStorage.getItem("auth_token")) {
        store.dispatch("userModule/logout");
        next({
          name: "Index",
        });
        return;
      }
      store.dispatch("userModule/getUserProfile");
      next();
    } else if (
      !to.matched.some((record) => record.meta.requiresAuth) &&
      to.matched.some((record) => record.meta.requiresAuthCheck)
    ) {
      // if !reqAuth & reqAuthCheck => { if auth_token != undefined => checkToken { if checkResult === false => { logout } else { Next() }}
      if (localStorage.getItem("auth_token")) {
        await store.dispatch("userModule/getUserProfile");
      }
      next();
    } else if (
      !to.matched.some((record) => record.meta.requiresAuth) &&
      !to.matched.some((record) => record.meta.requiresAuthCheck)
    ) {
      if (localStorage.getItem("auth_token")) {
        store.dispatch("userModule/logout");
        next({
          name: "Index",
        });
        return;
      }
      next();
    } else {
      next();
    }
  });
  routerInstance = Router;
  return Router;
});
export { routerInstance };
