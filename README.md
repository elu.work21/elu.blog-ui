# Quasar App (elu.blog)

A Quasar Framework app

## Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
yarn dev
```

### Lint the files

```bash
yarn lint
```

### Format the files

```bash
yarn format
```

### Build the app for production

```bash
quasar build
```

### Credentials

```js
{
  name: "elu",
  email: "elu.work21@gmail.com",
  password: 'passw0rd$01081337'
}
```
